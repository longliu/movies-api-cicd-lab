import express from 'express';
import Review from "./reviewModel";

const router = express.Router();

router.post('/', async (req, res) => {
    try {
        const newReview = new Review(req.body);
        const savedReview = await newReview.save();
        res.status(201).json({success: true, message: "Reviews is saved", code: 201});
    } catch (error) {
        res.status(400).json({success: false,message: error.message});
    }
});

router.get('/:movieId', async (req, res) => {
    try {
        const movieId = req.params.movieId;
        const reviews = await Review.find({movieId: movieId});
        if (!reviews) {
            return res.status(404).json({success: false, message: "No reviews found for this movie", code: 404});
        }
        res.status(200).json(reviews);
    } catch (error) {
        res.status(500).json({success: false,message: error.message});
    }
});
router.get('/', async (req, res) => {
    try {
        const reviews = await Review.find();
        res.status(200).json(reviews);
    } catch (error) {
        res.status(500).json({success: false,message: error.message});
    }
});

export default router;