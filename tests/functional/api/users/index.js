import chai from "chai";
import request from "supertest";

const mongoose = require("mongoose");
import User from "../../../../api/users/userModel";
import api from "../../../../index";

const expect = chai.expect;
let db;
let user1token;
let userId;

describe("Users endpoint", () => {
    before(() => {
        mongoose.connect(process.env.MONGO_DB, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        db = mongoose.connection;
    });

    after(async () => {
        try {
            await db.dropDatabase();
        } catch (error) {
            console.log(error);
        }
    });
    beforeEach(async () => {
        try {
            await User.deleteMany();
            // Register two users
            await request(api).post("/api/users?action=register").send({
                username: "user1",
                email: 'user1@example.com',
                password: "test123@",
            });
            await request(api).post("/api/users?action=register").send({
                username: "user2",
                email: 'user2@example.com',
                password: "test123@",
            });
        } catch (err) {
            console.error(`failed to Load user test Data: ${err}`);
        }
    });
    afterEach(() => {
        api.close();
    });
    describe("GET /api/users ", () => {
        it("should return the 2 users and a status 200", (done) => {
            request(api)
                .get("/api/users")
                .set("Accept", "application/json")
                .expect(200)
                .end((err, res) => {
                    expect(res.body).to.be.a("array");
                    expect(res.body.length).to.equal(2);
                    let result = res.body.map((user) => user.username);
                    expect(result).to.have.members(["user1", "user2"]);
                    done();
                });
        });
    });

    describe("POST /api/users ", () => {
        describe("For a register action", () => {
            describe("when the payload is error", () => {
                it("should return a 401 status and the error message", () => {
                    return request(api)
                        .post("/api/users?action=register")
                        .send({
                            username: "user1",
                            email: 'user1@example.com',
                            password: "test123@",
                        })
                        .expect(401)
                        .expect({success: false, msg: 'Registration failed. User\'s name has been used.', code: 401});
                });
                after(() => {
                    return request(api)
                        .get("/api/users")
                        .set("Accept", "application/json")
                        .expect(200)
                        .then((res) => {
                            expect(res.body.length).to.equal(2);
                            const result = res.body.map((user) => user.username);
                            expect(result).to.have.members(["user1", "user2"]);
                        });
                });
            });
            describe("when the payload is correct", () => {
                it("should return a 201 status and the confirmation message", () => {
                    return request(api)
                        .post("/api/users?action=register")
                        .send({
                            username: "user3",
                            email: 'user3@example.com',
                            password: "test123@",
                        })
                        .expect(201)
                        .expect({success: true, msg: 'Successful created new user.', code: 201});
                });
                after(() => {
                    return request(api)
                        .get("/api/users")
                        .set("Accept", "application/json")
                        .expect(200)
                        .then((res) => {
                            expect(res.body.length).to.equal(3);
                            const result = res.body.map((user) => user.username);
                            expect(result).to.have.members(["user1", "user2", "user3"]);
                        });
                });
            });
        });
        describe("For an authenticate action", () => {
            describe("when the payload is correct", () => {
                it("should return a 200 status and a generated token", () => {
                    return request(api)
                        .post("/api/users?action=authenticate")
                        .send({
                            username: "user1",
                            password: "test123@",
                        })
                        .expect(200)
                        .then((res) => {
                            expect(res.body.success).to.be.true;
                            expect(res.body.token).to.not.be.undefined;
                            user1token = res.body.token.substring(7);
                        });
                });
            });
            describe("when the payload is error", () => {
                it("should return a 401 status and user not found", () => {
                    return request(api)
                        .post("/api/users?action=authenticate")
                        .send({
                            username: "user1234",
                            password: "test123@",
                        })
                        .expect(401)
                        .expect({success: false, msg: 'Authentication failed. User not found.', code: 401});
                });
                it("should return a 401 status and wrong password", () => {
                    return request(api)
                        .post("/api/users?action=authenticate")
                        .send({
                            username: "user1",
                            password: "test",
                        })
                        .expect(401)
                        .expect({success: false, msg: 'Wrong password.', code: 401});
                });
            });
        });
        describe("For a googleLogin action", () => {
            describe("when the payload is correct", () => {
                it("should return a 201 status and create google user", () => {
                    return request(api)
                        .post("/api/users?action=googleLogin")
                        .send({
                            username: "user1-google",
                            email: 'user1@gmail.com',
                            password: "google@123",
                        })
                        .expect(201)
                        .expect({success: true, msg: 'Successful created new google user.', code: 201});
                });
                after(() => {
                    return request(api)
                        .get("/api/users")
                        .set("Accept", "application/json")
                        .expect(200)
                        .then((res) => {
                            expect(res.body.length).to.equal(3);
                            const result = res.body.map((user) => user.username);
                            expect(result).to.have.members(["user1", "user2", "user1-google"]);
                        });
                });
            });
        });
    });
});
describe("Users profile endpoint", () => {
    before(() => {
        mongoose.connect(process.env.MONGO_DB, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        db = mongoose.connection;
    });

    after(async () => {
        try {
            await db.dropDatabase();
        } catch (error) {
            console.log(error);
        }
    });
    beforeEach(async () => {
        try {
            await User.deleteMany();
            await request(api).post("/api/users?action=register").send({
                username: "user1",
                email: 'user1@example.com',
                password: "test123@",
                playlist: [572802, 572803],
                favourites: [572802, 572803],
                follows: [787699, 787698],
            });
            const res = await request(api)
                .post("/api/users?action=authenticate")
                .send({
                    username: "user1",
                    password: "test123@",
                });
            userId = res.body.userId;
        } catch (err) {
            console.error(`failed to Load user test Data: ${err}`);
        }
    });
    afterEach(() => {
        api.close();
    });
    describe("GET /api/users/.../:userId ", () => {
        describe("For playlist", () => {
            describe("when the userId is error", () => {
                it("should return a 401 status and the error message", () => {
                    return request(api)
                        .get(`/api/users/playlist/6585bbe95e30b8ea772b84fb`)
                        .expect(404)
                        .expect({success: false, msg: "User not found", code: 404});
                });
            });
            describe("when the userId is correct", () => {
                it("should return a 200 status and playlist", () => {
                    return request(api)
                        .get(`/api/users/playlist/${userId}`)
                        .set("Accept", "application/json")
                        .expect(200)
                        .then((res) => {
                            const result = res.body.playlist;
                            expect(result.length).to.equal(2);
                            expect(result).to.have.members([572802, 572803]);
                        });
                });
            });
        });
        describe("For favorites", () => {
            describe("when the userId is error", () => {
                it("should return a 404 status and the error message", () => {
                    return request(api)
                        .get(`/api/users/favourites/6585bbe95e30b8ea772b84fb`)
                        .expect(404)
                        .expect({success: false, msg: "User not found", code: 404});
                });
            });
            describe("when the userId is correct", () => {
                it("should return a 200 status and favourites", () => {
                    return request(api)
                        .get(`/api/users/favourites/${userId}`)
                        .set("Accept", "application/json")
                        .expect(200)
                        .then((res) => {
                            const result = res.body.favourites;
                            expect(result.length).to.equal(2);
                            expect(result).to.have.members([572802, 572803]);
                        });
                });
            });
        });
        describe("For follows", () => {
            describe("when the userId is error", () => {
                it("should return a 404 status and the error message", () => {
                    return request(api)
                        .get(`/api/users/follows/6585bbe95e30b8ea772b84fb`)
                        .expect(404)
                        .expect({success: false, msg: "User not found", code: 404});
                });
            });
            describe("when the userId is correct", () => {
                it("should return a 200 status and follows", () => {
                    return request(api)
                        .get(`/api/users/follows/${userId}`)
                        .set("Accept", "application/json")
                        .expect(200)
                        .then((res) => {
                            const result = res.body.follows;
                            expect(result.length).to.equal(2);
                            expect(result).to.have.members([787699, 787698]);
                        });
                });
            });
        });
    });
    describe("POST /api/users/.../:userId ", () => {
        describe("For playlist", () => {
            describe("when the user id is error", () => {
                it("should return a 404 status and the error message", () => {
                    return request(api)
                        .post(`/api/users/playlist/6585bbe95e30b8ea772b84fb`)
                        .send({
                            movieId: 572802,
                        })
                        .expect(404)
                        .expect({success: false, msg: "User not found", code: 404});
                });
            });
            describe("when the movies id is lost", () => {
                it("should return a 400 status and the error message", () => {
                    return request(api)
                        .post(`/api/users/playlist/${userId}`)
                        .send({})
                        .expect(400)
                        .expect({success: false, message: 'Movie ID is required.', code: 400});
                });
            });
            describe("when the movies has been existed", () => {
                it("should return a 401 status and the error message", () => {
                    return request(api)
                        .post(`/api/users/playlist/${userId}`)
                        .send({
                            movieId: 572802,
                        })
                        .expect(401)
                        .expect({success: false, message: 'Movie already in playlist.', code: 401});
                });
            });
            describe("when all values is ok", () => {
                it("should return a 200 status ", () => {
                    return request(api)
                        .post(`/api/users/playlist/${userId}`)
                        .send({
                            movieId: 787699,
                        })
                        .expect(200)
                        .expect({success: true, message: 'Movie added to playlist.', code: 200});
                });
                after(() => {
                    return request(api)
                        .get(`/api/users/playlist/${userId}`)
                        .set("Accept", "application/json")
                        .expect(200)
                        .then((res) => {
                            const result = res.body.playlist;
                            expect(result.length).to.equal(3);
                            expect(result).to.have.members([572802, 572803, 787699]);
                        });
                });
            });
        });
        describe("For favourites", () => {
            describe("when the user id is error", () => {
                it("should return a 404 status and the error message", () => {
                    return request(api)
                        .post(`/api/users/favourites/6585bbe95e30b8ea772b84fb`)
                        .send({
                            movieId: 572802,
                        })
                        .expect(404)
                        .expect({success: false, msg: "User not found", code: 404});
                });
            });
            describe("when the movies id is lost", () => {
                it("should return a 400 status and the error message", () => {
                    return request(api)
                        .post(`/api/users/favourites/${userId}`)
                        .send({})
                        .expect(400)
                        .expect({success: false, message: 'Movie ID is required.', code: 400});
                });
            });
            describe("when the movies has been existed", () => {
                it("should return a 401 status and the error message", () => {
                    return request(api)
                        .post(`/api/users/favourites/${userId}`)
                        .send({
                            movieId: 572802,
                        })
                        .expect(401)
                        .expect({success: false, message: 'Movie already in favourites.', code: 401});
                });
            });
            describe("when all values is ok", () => {
                it("should return a 200 status ", () => {
                    return request(api)
                        .post(`/api/users/favourites/${userId}`)
                        .send({
                            movieId: 787699,
                        })
                        .expect(200)
                        .expect({success: true, message: 'Movie added to favourites.', code: 200});
                });
                after(() => {
                    return request(api)
                        .get(`/api/users/favourites/${userId}`)
                        .set("Accept", "application/json")
                        .expect(200)
                        .then((res) => {
                            const result = res.body.favourites;
                            expect(result.length).to.equal(3);
                            expect(result).to.have.members([572802, 572803, 787699]);
                        });
                });
            });
        });
        describe("For follows", () => {
            describe("when the user id is error", () => {
                it("should return a 404 status and the error message", () => {
                    return request(api)
                        .post(`/api/users/follows/6585bbe95e30b8ea772b84fb`)
                        .send({
                            personId: 787699,
                        })
                        .expect(404)
                        .expect({success: false, msg: "User not found", code: 404});
                });
            });
            describe("when the person id is lost", () => {
                it("should return a 400 status and the error message", () => {
                    return request(api)
                        .post(`/api/users/follows/${userId}`)
                        .send({})
                        .expect(400)
                        .expect({success: false, message: 'Person ID is required.', code: 400});
                });
            });
            describe("when the person has been existed", () => {
                it("should return a 401 status and the error message", () => {
                    return request(api)
                        .post(`/api/users/follows/${userId}`)
                        .send({
                            personId: 787699,
                        })
                        .expect(401)
                        .expect({success: false, message: 'person already in follows.', code: 401});
                });
            });
            describe("when all values is ok", () => {
                it("should return a 200 status ", () => {
                    return request(api)
                        .post(`/api/users/follows/${userId}`)
                        .send({
                            personId: 787697,
                        })
                        .expect(200)
                        .expect({success: true, message: 'Person followed.', code: 200});
                });
                after(() => {
                    return request(api)
                        .get(`/api/users/follows/${userId}`)
                        .set("Accept", "application/json")
                        .expect(200)
                        .then((res) => {
                            const result = res.body.follows;
                            expect(result.length).to.equal(3);
                            expect(result).to.have.members([787699, 787698, 787697]);
                        });
                });
            });
        });
    });
    describe("DELETE /api/users/.../:userId ", () => {
        describe("For playlist", () => {
            describe("when the user id is error", () => {
                it("should return a 404 status and the error message", () => {
                    return request(api)
                        .delete(`/api/users/playlist/6585bbe95e30b8ea772b84fb`)
                        .send({
                            movieId: 572802,
                        })
                        .expect(404)
                        .expect({success: false, msg: "User not found", code: 404});
                });
            });
            describe("when the movies id is lost", () => {
                it("should return a 400 status and the error message", () => {
                    return request(api)
                        .delete(`/api/users/playlist/${userId}`)
                        .send({})
                        .expect(400)
                        .expect({success: false, message: 'Movie ID is required.', code: 400});
                });
            });
            describe("when the movies is not in playlist", () => {
                it("should return a 401 status and the error message", () => {
                    return request(api)
                        .delete(`/api/users/playlist/${userId}`)
                        .send({
                            movieId: 572804,
                        })
                        .expect(401)
                        .expect({success: false, message: 'Movie is not in playlist.', code: 401});
                });
            });
            describe("when all values is ok", () => {
                it("should return a 200 status ", () => {
                    return request(api)
                        .delete(`/api/users/playlist/${userId}`)
                        .send({
                            movieId: 572803,
                        })
                        .expect(200)
                        .expect({success: true, message: 'Movie removed from playlist.', code: 200});
                });
                after(() => {
                    return request(api)
                        .get(`/api/users/playlist/${userId}`)
                        .set("Accept", "application/json")
                        .expect(200)
                        .then((res) => {
                            const result = res.body.playlist;
                            expect(result.length).to.equal(1);
                            expect(result).to.have.members([572802]);
                        });
                });
            });
        });
        describe("For favourites", () => {
            describe("when the user id is error", () => {
                it("should return a 404 status and the error message", () => {
                    return request(api)
                        .delete(`/api/users/favourites/6585bbe95e30b8ea772b84fb`)
                        .send({
                            movieId: 572802,
                        })
                        .expect(404)
                        .expect({success: false, msg: "User not found", code: 404});
                });
            });
            describe("when the movies id is lost", () => {
                it("should return a 400 status and the error message", () => {
                    return request(api)
                        .delete(`/api/users/favourites/${userId}`)
                        .send({})
                        .expect(400)
                        .expect({success: false, message: 'Movie ID is required.', code: 400});
                });
            });
            describe("when the movies is not in favourites", () => {
                it("should return a 401 status and the error message", () => {
                    return request(api)
                        .delete(`/api/users/favourites/${userId}`)
                        .send({
                            movieId: 572804,
                        })
                        .expect(401)
                        .expect({success: false, message: 'Movie is not in favourites.', code: 401});
                });
            });
            describe("when all values is ok", () => {
                it("should return a 200 status ", () => {
                    return request(api)
                        .delete(`/api/users/favourites/${userId}`)
                        .send({
                            movieId: 572803,
                        })
                        .expect(200)
                        .expect({success: true, message: 'Movie removed from favourites.', code: 200});
                });
                after(() => {
                    return request(api)
                        .get(`/api/users/favourites/${userId}`)
                        .set("Accept", "application/json")
                        .expect(200)
                        .then((res) => {
                            const result = res.body.favourites;
                            expect(result.length).to.equal(1);
                            expect(result).to.have.members([572802]);
                        });
                });
            });
        });
        describe("For follows", () => {
            describe("when the user id is error", () => {
                it("should return a 404 status and the error message", () => {
                    return request(api)
                        .delete(`/api/users/follows/6585bbe95e30b8ea772b84fb`)
                        .send({
                            personId: 787699,
                        })
                        .expect(404)
                        .expect({success: false, msg: "User not found", code: 404});
                });
            });
            describe("when the person id is lost", () => {
                it("should return a 400 status and the error message", () => {
                    return request(api)
                        .delete(`/api/users/follows/${userId}`)
                        .send({})
                        .expect(400)
                        .expect({success: false, message: 'Person ID is required.', code: 400});
                });
            });
            describe("when the person is not in follows", () => {
                it("should return a 401 status and the error message", () => {
                    return request(api)
                        .delete(`/api/users/follows/${userId}`)
                        .send({
                            personId: 787697,
                        })
                        .expect(401)
                        .expect({success: false, message: 'person is not in follows.', code: 401});
                });
            });
            describe("when all values is ok", () => {
                it("should return a 200 status ", () => {
                    return request(api)
                        .delete(`/api/users/follows/${userId}`)
                        .send({
                            personId: 787698,
                        })
                        .expect(200)
                        .expect({success: true, message: 'Person unfollowed.', code: 200});
                });
                after(() => {
                    return request(api)
                        .get(`/api/users/follows/${userId}`)
                        .set("Accept", "application/json")
                        .expect(200)
                        .then((res) => {
                            const result = res.body.follows;
                            expect(result.length).to.equal(1);
                            expect(result).to.have.members([787699]);
                        });
                });
            });
        });
    });
});