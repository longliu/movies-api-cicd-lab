import chai from "chai";
import request from "supertest";
import api from "../../../../index";

const expect = chai.expect;
let page = 1;

describe("Movies endpoint", () => {
    afterEach(() => {
        api.close(); // Release PORT 8080
    });
    describe("GET /api/movies/tmdb ", () => {
        it("should return 20 movies and a status 200", (done) => {
            request(api)
                .get(`/api/movies/tmdb?page=${page}`)
                .set("Accept", "application/json")
                .expect(200)
                .end((err, res) => {
                    expect(res.body.results).to.be.a("array");
                    expect(res.body.results.length).to.equal(20);
                    done();
                });
        });
    });
    describe("GET /api/movies/tmdb/popular ", () => {
        it("should return 20 popular movies and a status 200", (done) => {
            request(api)
                .get(`/api/movies/tmdb/popular?page=${page}`)
                .set("Accept", "application/json")
                .expect(200)
                .end((err, res) => {
                    expect(res.body.results).to.be.a("array");
                    expect(res.body.results.length).to.equal(20);
                    done();
                });
        });
    });
    describe("GET /api/movies/tmdb/toprated ", () => {
        it("should return 20 top-rated movies and a status 200", (done) => {
            request(api)
                .get(`/api/movies/tmdb/toprated?page=${page}`)
                .set("Accept", "application/json")
                .expect(200)
                .end((err, res) => {
                    expect(res.body.results).to.be.a("array");
                    expect(res.body.results.length).to.equal(20);
                    done();
                });
        });
    });
    describe("GET /api/movies/tmdb/trending ", () => {
        it("should return 20 trending movies and a status 200", (done) => {
            request(api)
                .get(`/api/movies/tmdb/trending?page=${page}`)
                .set("Accept", "application/json")
                .expect(200)
                .end((err, res) => {
                    expect(res.body.results).to.be.a("array");
                    expect(res.body.results.length).to.equal(20);
                    done();
                });
        });
    });
    describe("GET /api/movies/tmdb/upcoming ", () => {
        it("should return 20 upcoming movies and a status 200", (done) => {
            request(api)
                .get(`/api/movies/tmdb/upcoming?page=${page}`)
                .set("Accept", "application/json")
                .expect(200)
                .end((err, res) => {
                    expect(res.body.results).to.be.a("array");
                    expect(res.body.results.length).to.equal(20);
                    done();
                });
        });
    });
    describe("GET /api/movies/tmdb/genres ", () => {
        it("should return movie genres and a status 200", (done) => {
            request(api)
                .get(`/api/movies/tmdb/genres`)
                .set("Accept", "application/json")
                .expect(200)
                .end((err, res) => {
                    expect(res.body.genres).to.be.a("array");
                    expect(res.body.genres.length).to.equal(19);
                    done();
                });
        });
    });
    describe("GET /api/movies/tmdb/:id", () => {
        it("should return the matching movie", () => {
            return request(api)
                .get(`/api/movies/tmdb/695721`)
                .set("Accept", "application/json")
                .expect(200)
                .then((res) => {
                    expect(res.body).to.have.property("original_title", "The Hunger Games: The Ballad of Songbirds & Snakes");
                });
        });
    });
    describe("GET /api/movies/tmdb/img/:id", () => {
        it("should return the matching movie images", () => {
            return request(api)
                .get(`/api/movies/tmdb/img/695721`)
                .set("Accept", "application/json")
                .expect(200)
                .then((res) => {
                    expect(res.body.backdrops[0]).to.have.property("file_path");
                });
        });
    });
    describe("GET /api/movies/tmdb/reviews/:id", () => {
        it("should return the matching movie reviews", () => {
            return request(api)
                .get(`/api/movies/tmdb/reviews/695721`)
                .set("Accept", "application/json")
                .expect(200)
                .then((res) => {
                    expect(res.body.results[0]).to.have.property("content");
                });
        });
    });
    describe("GET /api/movies/tmdb/recommendations/:id", () => {
        it("should return the matching recommendations movie", () => {
            return request(api)
                .get(`/api/movies/tmdb/recommendations/695721`)
                .set("Accept", "application/json")
                .expect(200)
                .then((res) => {
                    expect(res.body).to.have.property("results");
                });
        });
    });
    describe("GET /api/movies/tmdb/similar/:id", () => {
        it("should return the matching similar movie", () => {
            return request(api)
                .get(`/api/movies/tmdb/similar/695721`)
                .set("Accept", "application/json")
                .expect(200)
                .then((res) => {
                    expect(res.body).to.have.property("results");
                });
        });
    });
    describe("GET /api/movies/tmdb/actor/:id", () => {
        it("should return the matching actor", () => {
            return request(api)
                .get(`/api/movies/tmdb/actor/1548301`)
                .set("Accept", "application/json")
                .expect(200)
                .then((res) => {
                    expect(res.body).to.have.property("name", "Farrah Mackenzie");
                });
        });
    });
    describe("GET /api/movies/tmdb/actor/img/:id", () => {
        it("should return the matching actor's images", () => {
            return request(api)
                .get(`/api/movies/tmdb/actor/img/1548301`)
                .set("Accept", "application/json")
                .expect(200)
                .then((res) => {
                    expect(res.body.profiles[0]).to.have.property("file_path");
                });
        });
    });
    describe("GET /api/movies/tmdb/credits/:id", () => {
        it("should return the relational actors", () => {
            return request(api)
                .get(`/api/movies/tmdb/credits/695721`)
                .set("Accept", "application/json")
                .expect(200)
                .then((res) => {
                    expect(res.body.cast[0]).to.have.property("name", "Tom Blyth");
                });
        });
    });
    describe("GET /api/movies/tmdb/movieCredits/:id", () => {
        it("should return the relational movies", () => {
            return request(api)
                .get(`/api/movies/tmdb/movieCredits/1548301`)
                .set("Accept", "application/json")
                .expect(200)
                .then((res) => {
                    expect(res.body.cast[0]).to.have.property("id", 633921);
                });
        });
    });
});
