import chai from "chai";
import request from "supertest";

const mongoose = require("mongoose");
import Review from "../../../../api/reviews/reviewModel";
import api from "../../../../index";

const expect = chai.expect;
let db;


describe("Reviews endpoint", () => {
    before(() => {
        mongoose.connect(process.env.MONGO_DB, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        db = mongoose.connection;
    });

    after(async () => {
        try {
            await db.dropDatabase();
        } catch (error) {
            console.log(error);
        }
    });
    beforeEach(async () => {
        try {
            await Review.deleteMany();
            await request(api).post("/api/reviews").send({
                author: 'user1',
                review: 'asfsfsmwwofieowfwojeofjwfew',
                rating: 5,
                movieId: 572802,
            });
            await request(api).post("/api/reviews").send({
                author: 'user2',
                review: 'asfsfsmwwofieowfwojeofjwfvsddzvdZVew',
                rating: 3,
                movieId: 572845,
            });
        } catch (err) {
            console.error(`failed to Load review test Data: ${err}`);
        }
    });
    afterEach(() => {
        api.close();
    });
    describe("GET /api/reviews ", () => {
        describe("get all reviews ", () => {
            it("should return the 2 reviews and a status 200", (done) => {
                request(api)
                    .get("/api/reviews")
                    .set("Accept", "application/json")
                    .expect(200)
                    .end((err, res) => {
                        expect(res.body).to.be.a("array");
                        expect(res.body.length).to.equal(2);
                        let result = res.body.map((review) => review.author);
                        expect(result).to.have.members(["user1", "user2"]);
                        done();
                    });
            });
        });
        describe("get reviews by movie id ", () => {
            describe("movie id is existed", () => {
                it("should return the 1 reviews and a status 200", (done) => {
                    request(api)
                        .get(`/api/reviews/572802`)
                        .set("Accept", "application/json")
                        .expect(200)
                        .end((err, res) => {
                            expect(res.body).to.be.a("array");
                            expect(res.body.length).to.equal(1);
                            let result = res.body.map((review) => review.author);
                            expect(result).to.have.members(["user1"]);
                            done();
                        });
                });
            });
            describe("movie id is not existed", () => {
                it("should return status 404 and error message", (done) => {
                    request(api)
                        .get(`/api/reviews/572802`)
                        .set("Accept", "application/json")
                        .expect(404)
                        .expect({success: false, message: "No reviews found for this movie", code: 404});
                    done();
                });
            });
        });
    });
    describe("POST /api/reviews", () => {
        it("should return status 201 ", (done) => {
            request(api)
                .post(`/api/reviews`)
                .send({
                    author: 'user3',
                    review: 'erbbesbesabeasabvesbv',
                    rating: 4,
                    movieId: 572845,
                })
                .set("Accept", "application/json")
                .expect(201)
                .expect({success: true, message: "Reviews is saved", code: 201});
            request(api)
                .get("/api/reviews")
                .set("Accept", "application/json")
                .expect(200)
                .then((res) => {
                    expect(res.body).to.be.a("array");
                    expect(res.body.length).to.equal(3);
                    let result = res.body.map((review) => review.author);
                    expect(result).to.have.members(["user1", "user2", "user3"]);
                });
            done();
        });
    });
});