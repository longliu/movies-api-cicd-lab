# Assignment 2 - Agile Software Practice.

Name: long liu

## API endpoints.

https://app.swaggerhub.com/apis/20104729/movies-asp-ca_2/1.0
The link can access my API document on Swagger hub.

+ GET api/users - get all users.
+ POST api/users?action=register - register a new user.
+ put api/users/:id - change a specific user by id.
+ GET api/users/playlist/:id - get all movies in playlist of a specific user by id.
+ POST api/users/playlist/:id - add a movie to the playlist of a specific user by id.
+ DELETE api/users/playlist/:id - remove a movie to the playlist of a specific user by id.
+ GET api/users/favourites/:id - get all movies in favourites of a specific user by id.
+ POST api/users/favourites/:id - add a movie to the favourites of a specific user by id.
+ DELETE api/users/favourites/:id - remove a movie to the favourites of a specific user by id.
+ GET api/users/follows/:id - get all actors in follows of a specific user by id.
+ POST api/users/follows/:id - add an actor to the follows of a specific user by id.
+ DELETE api/users/follows/:id - remove an actor to the follows of a specific user by id
+ GET api/reviews - get all reviews.
+ GET api/reviews - add a review into database.
+ GET api/reviews/:id - get reviews by movie id.
+ GET api/movies/tmdb?page=1 - get discover movies.
+ GET api/movies/tmdb/popular?page=1 - get popular movies.
+ GET api/movies/tmdb/toprated?page=1 - get top-rated movies.
+ GET api/movies/tmdb/trending?page=1 - get daily trending movies.
+ GET api/movies/tmdb/upcoming?page=1 - get upcoming movies.
+ GET api/movies/tmdb/genres - get movies' genres.
+ GET api/movies/tmdb/:id - get details of a specific movie.
+ GET api/movies/tmdb/img/:id - get images of a specific movie.
+ GET api/movies/tmdb/reviews/:id - get reviews of a specific movie.
+ GET api/movies/tmdb/recommendations?page=1 - get recommendations movies rely on a specific movie.
+ GET api/movies/tmdb/similar?page=1 - get similar movies rely on a specific movie.
+ GET api/movies/tmdb/actor/:id - get details of a specific actor.
+ GET api/movies/tmdb/actor/img/:id - get images of a specific actor.
+ GET api/movies/tmdb/credits/:id - get credits of a specific movie.
+ GET api/movies/tmdb/movieCredits/:id - get movies that have something of a specific actor.


## Automated Testing.

~~~
    Users endpoint
    GET /api/users 
      ✓ should return the 2 users and a status 200
    POST /api/users 
      For a register action
        when the payload is error
          ✓ should return a 401 status and the error message
        when the payload is correct
          ✓ should return a 201 status and the confirmation message (62ms)
      For an authenticate action
        when the payload is correct
          ✓ should return a 200 status and a generated token (61ms)
        when the payload is error
          ✓ should return a 401 status and user not found
          ✓ should return a 401 status and wrong password (58ms)
      For a googleLogin action
        when the payload is correct
          ✓ should return a 201 status and create google user (61ms)
  Users profile endpoint
    GET /api/users/.../:userId 
      For playlist
        when the userId is error
          ✓ should return a 401 status and the error message
        when the userId is correct
          ✓ should return a 200 status and playlist
      For favorites
        when the userId is error
          ✓ should return a 404 status and the error message
        when the userId is correct
          ✓ should return a 200 status and favourites
      For follows
        when the userId is error
          ✓ should return a 404 status and the error message
        when the userId is correct
          ✓ should return a 200 status and follows
    POST /api/users/.../:userId 
      For playlist
        when the user id is error
          ✓ should return a 404 status and the error message
        when the movies id is lost
          ✓ should return a 400 status and the error message
        when the movies has been existed
          ✓ should return a 401 status and the error message
        when all values is ok
          ✓ should return a 200 status 
      For favourites
        when the user id is error
          ✓ should return a 404 status and the error message
        when the movies id is lost
          ✓ should return a 400 status and the error message
        when the movies has been existed
          ✓ should return a 401 status and the error message
        when all values is ok
          ✓ should return a 200 status 
      For follows
        when the user id is error
          ✓ should return a 404 status and the error message
        when the person id is lost
          ✓ should return a 400 status and the error message
        when the person has been existed
          ✓ should return a 401 status and the error message
        when all values is ok
          ✓ should return a 200 status 
    DELETE /api/users/.../:userId 
      For playlist
        when the user id is error
          ✓ should return a 404 status and the error message
        when the movies id is lost
          ✓ should return a 400 status and the error message
        when the movies is not in playlist
          ✓ should return a 401 status and the error message
        when all values is ok
          ✓ should return a 200 status 
      For favourites
        when the user id is error
          ✓ should return a 404 status and the error message
        when the movies id is lost
          ✓ should return a 400 status and the error message
        when the movies is not in favourites
          ✓ should return a 401 status and the error message
        when all values is ok
          ✓ should return a 200 status 
      For follows
        when the user id is error
          ✓ should return a 404 status and the error message
        when the person id is lost
          ✓ should return a 400 status and the error message
        when the person is not in follows
          ✓ should return a 401 status and the error message
        when all values is ok
          ✓ should return a 200 status 
  Movies endpoint
    GET /api/movies/tmdb 
      ✓ should return 20 movies and a status 200 (114ms)
    GET /api/movies/tmdb/popular 
      ✓ should return 20 popular movies and a status 200 (59ms)
    GET /api/movies/tmdb/toprated 
      ✓ should return 20 top-rated movies and a status 200
    GET /api/movies/tmdb/trending 
      ✓ should return 20 trending movies and a status 200 (59ms)
    GET /api/movies/tmdb/upcoming 
      ✓ should return 20 upcoming movies and a status 200 (52ms)
    GET /api/movies/tmdb/genres 
      ✓ should return movie genres and a status 200
    GET /api/movies/tmdb/:id
      ✓ should return the matching movie
    GET /api/movies/tmdb/img/:id
      ✓ should return the matching movie images (51ms)
    GET /api/movies/tmdb/reviews/:id
      ✓ should return the matching movie reviews (40ms)
    GET /api/movies/tmdb/recommendations/:id
      ✓ should return the matching recommendations movie (57ms)
    GET /api/movies/tmdb/similar/:id
      ✓ should return the matching similar movie
    GET /api/movies/tmdb/actor/:id
      ✓ should return the matching actor
    GET /api/movies/tmdb/actor/img/:id
      ✓ should return the matching actor's images
    GET /api/movies/tmdb/credits/:id
      ✓ should return the relational actors
    GET /api/movies/tmdb/movieCredits/:id
      ✓ should return the relational movies
  52 passing (6s)
~~~

## Deployments.

[![Netlify Status](https://api.netlify.com/api/v1/badges/0e0d92e0-afdc-458d-b65d-533670642140/deploy-status)](https://app.netlify.com/sites/jovial-puffpuff-2391d2/deploys?branch=develop)

https://movies-api-staging-ll-6bcfee1aa0fa.herokuapp.com/ deployed to Heroku

## Independent Learning

Use the [Istanbul](https://istanbul.js.org/) npm package to generate Code Coverage reports

[![Coverage Status](https://coveralls.io/repos/gitlab/longliu/movies-api-cicd-lab/badge.svg?branch=)](https://coveralls.io/gitlab/longliu/movies-api-cicd-lab?branch=)

Netlify deploy